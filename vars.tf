variable "project_name" {
  type = string
  default = "terraform-338212"
}

variable "ssh_k" {
  type = list(object({
    publickey = string
    user = string
  }))
  description = "list of public ssh keys that have access to the VM"
  default = [
      {
        user = "gabrielrosadias"
        publickey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDNRLCIVHXCoB1tRUgW73zqMYirMaIvAFVlFWY7bS0OOkRskAICqYZl7S1iEa4+qkij1/hpmqTf3ifK6uhdOI/8nRWzgR6dFO0tmppslcKMppTBgkrzUgUatLCv2l3gDMzAcsE3MdgnErnCdgkIDgOimElzfSlzD5hNHq7KwgZe3NllJeRFzCzhK9sPJAYNRMo87/Mxv+WjwOVD6HY7McF8HqYDpE686B7fBz7sXYslwzIaeA3BQn+F906DnwfGmn3SAFN9Md9wGvgJTtzJ7wkDSTIvuydWm8JR+5hOMuV1mokKN0SzRSHv3IfvwN2ud92/zur/EBhem7TujP6y4hVkoFe0FXQCljvCJ9IopdiDIiWPZUcYTg6J4YqEN3h8fnx0a2pR+f+O6x3D0KxExmAp+YmnD7y8H/zISuxjrdHQ2BD1Axuw427JTVn8u79FOOD7JPw/zyJfQS4Sk90zp126KkiCSm9/iHkRRDJp2VeJpT671Wx9EKeRv9oxQmppq8E= gabrielrosadias@gmail.com"
      }
  ]
}